variable "project_name" {
  type    = string
  default = "janes-terraform-test-locking"
}

variable "aws_default_region" {
  type    = string
  default = "eu-west-2"
}

variable "env_name" {
  type    = string
  default = "management-account"
}