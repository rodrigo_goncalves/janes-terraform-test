provider "aws" {
  region = var.aws_default_region
}

provider "random" {}

resource "random_id" "id" {
  byte_length = 8
}

output "state_bucket_name" {
    value = module.terraform_state_bucket.s3_bucket_id
}

output "dynamodb_state_locking_table" {
    value = module.terraform_locking_dynamo_table.dynamodb_table_id
}