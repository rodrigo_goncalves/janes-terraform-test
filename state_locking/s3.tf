module "terraform_state_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "${var.project_name}-${random_id.id.hex}"
  acl    = "private"

  versioning = {
    enabled = true
  }

  tags = {
    Terraform   = "true"
    Environment = var.env_name
  }
}