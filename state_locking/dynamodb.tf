module "terraform_locking_dynamo_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"

  name     = "${var.project_name}-${random_id.id.hex}"
  hash_key = "LockID"

  attributes = [
    {
      name = "LockID"
      type = "S"
    }
  ]

  tags = {
    Terraform   = "true"
    Environment = var.env_name
  }

}