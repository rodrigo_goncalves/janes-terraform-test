module "alb_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "${var.project_name}-nginx"
  description = "Security group for ALB in front of Nginx EC2 instances with custom ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "Nginx ingress port"
      cidr_blocks = "86.18.98.38/32"
    }
  ]
}

module "ec2_nginx_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "${var.project_name}-nginx"
  description = "Security group for Nginx EC2 instances with custom ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port         = 80
      to_port           = 80
      protocol          = "tcp"
      description       = "Nginx ingress port"
      security_group_id = module.alb_sg.security_group_id
    }
  ]
}