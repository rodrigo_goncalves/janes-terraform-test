variable "launch_template_name" {
    type = string
}

variable "instance_ami" {
    type = string
}

variable "ec2_iam_role_id" {
    type = string
    description = "IAM Role ID to be assume by EC2 instance"
}

variable "availability_zone_placement" {
    type = string
    description = "Availability zone where instance should be provisioned"
}

variable "security_groups" {
    type = list
    description = "List of security groups instance should be associated with"
}

variable "env_name" {
    type = string
}

variable "user_data_file_location" {
    type = string
    description = "Best used with format '$${path.module}/example.sh'"
}