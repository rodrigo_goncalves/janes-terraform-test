resource "random_shuffle" "random_list" {
  input        = var.input_list
  result_count = var.result_count
}

output "result" {
    value = random_shuffle.random_list.result
}