variable "input_list" {
    type = list
    description = "List to be used by random_shuffle resource"
}

variable "result_count" {
    type = number
    description = "Number of results top return after performing random_shuffle on list"
}