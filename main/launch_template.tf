module "ec2_launch_template" {
  source = "./modules/janes_ec2_launch_template"

  launch_template_name        = "${var.env_name}-ec2-launch-template"
  availability_zone_placement = module.janes_random_shuffle.result
  instance_ami                = var.launch_template_ami
  security_groups             = [module.ec2_nginx_sg.security_group_id]
  env_name                    = var.env_name
  user_data_file_location     = "${path.module}/build_resources/ec2_user_data.sh"
  ec2_iam_role_id             = "EC2CloudWatch"
}   