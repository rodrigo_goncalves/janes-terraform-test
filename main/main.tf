provider "aws" {
  region = var.aws_default_region
}

provider "random" {}

resource "random_id" "id" {
  byte_length = 8
}