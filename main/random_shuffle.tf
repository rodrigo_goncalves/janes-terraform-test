module "janes_random_shuffle" {
  source = "./modules/janes_random_shuffle"

  input_list   = var.availability_zones
  result_count = 1
}