variable "project_name" {
  type    = string
  default = "janes-terraform-test"
}

variable "aws_default_region" {
  type    = string
  default = "eu-west-2"
}

variable "env_name" {
  type    = string
  default = "dev"
}

variable "main_vpc_name" {
  type    = string
  default = "main-vpc"
}

variable "availability_zones" {
  type    = list(any)
  default = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
}

variable "launch_template_ami" {
  type        = string
  description = "Default AMI: Amazon Linux 2023"
  default     = "ami-0520340324d0ea3bc"
}

variable "auto_scaling_instance_count" {
  type = number
  default = 2
}