terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "janes-terraform-test-locking-e110e89315f92f0a"
    dynamodb_table = "janes-terraform-test-locking-e110e89315f92f0a"
    key            = "terraform.tfstate"
    region         = "eu-west-2"
  }
}