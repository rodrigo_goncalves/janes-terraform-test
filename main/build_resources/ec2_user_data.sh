#!/bin/bash


amazon-linux-extras enable nginx1

yum clean metadata && sudo yum install -y nginx1

nginx_content_bucket = $(aws ssm get_parameter --name /janes/config/ec2/nginx_bucket_name)

aws s3 cp s3://<bucket name>/nginx.conf /etc/nginx/nginx.conf

systemctl enable nginx
systemctl start nginx